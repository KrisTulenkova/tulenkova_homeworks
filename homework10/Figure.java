package Homework09;

public abstract class Figure {
    public int x;
    protected int y;
    
    public Figure(int x, int y) {
       this.x = x;
       this.y = y;
    }
    
   abstract double getPerimeter();
}
