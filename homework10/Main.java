package Homework09;

public class Main {

    public static void main(String[] args) {

        Moving[] figures = { new Circle(3, 4, 5.0), new Square(1, 2, 6.3) };

        for (int i = 0; i < figures.length; i++) {
            figures[i].moveTo(11, 11);
        }
    }
}
