package Homework09;

public class Circle extends Ellipse implements Moving {
    private double radius;

    public Circle(int x, int y, double radius) {
        super(x, y, radius, radius);
    }

    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
