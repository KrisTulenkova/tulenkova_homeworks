package Homework09;

public class Square extends Rectangle implements Moving {

    public Square(int x, int y, double a) {
        super(x, y, a, a);
    }

    public double getPerimeter() {
        return 4 * a;
    }

    @Override
    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
