package Homework13;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Sequence sequence = new Sequence();
        int[] array = { 21, 50, 6, 17, 8, 9, 10, 22, 34, 67, 88 };

        int array1[] = sequence.filter(array, number -> number % 2 == 0);
        int array2[] = sequence.filter(array, number -> {
            int digitsSum = 0;
            while (number != 0) {
                int lastDigit = number % 10;
                digitsSum = digitsSum + lastDigit;
                number = number / 10;
            }
            return digitsSum % 2 != 0;
        });

        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
    }
}
