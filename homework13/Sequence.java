package Homework13;

import java.util.Arrays;


public class Sequence {
 
    public int[] filter(int[] array, ByCondition condition) {
        int size = 0;
        for (int i = 0; i < array.length; i++) {  
            if (condition.isOk(array[i]) == true) {
               
                array[size] = array[i];
                size++;

            }
        }
        return array = Arrays.copyOf(array, size);
    }
}

