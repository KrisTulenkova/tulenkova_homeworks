package Homework11;

public class Loger {
    private static final Loger instance;
    
    private  Loger() {
       
    }
    
    static{
        instance = new Loger();
    }
    void log(String message){
        System.out.println(message);
    }

    public static Loger getInstance() {
        return instance;
    }
}
