package Homework07;

import java.util.Scanner;

public class Homework07 {
    private static int number;
    private static int min = 0;

    public static void main(String[] args) {
        int[] b = new int[201];

        Scanner in = new Scanner(System.in);
        System.out.print("Input a number from -100 to 100: ");
        number = in.nextInt();
        while (number != -1) {
            number = number + 100;
            if (b[number] != 0) {
                b[number] = b[number] + 1;
            } else {
                b[number] = 1;
            }
            System.out.print("Input a number from -100 to 100: ");
            number = in.nextInt();
        }
        for (int i = 0; i < b.length - 1; i++) {
            if (b[i] != 0) {
                if (b[i] < b[i + 1]) {
                    min = i;
                }
            }
        }
        min = min - 100;
        System.out.println("The first number with the minimum number of repetitions: " + min);
    }
}
