package Homework08;

public class Human {
   private String name;
   private double weight;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }
    public static void getSort(Human [] human){
        for (int j = 0; j < human.length; j++) {
            double min = human[j].getWeight();
            int minIndex = j;
            for (int i = j + 1; i < human.length; i++) {
                if (human[i].getWeight() < min) {
                    min = human[i].getWeight();
                    minIndex = i;
                }
            }
            if (j != minIndex) {
                Human minWeight = human[j];
                human[j] = human[minIndex];
                human[minIndex] = minWeight;
            }
        }
    }
}
