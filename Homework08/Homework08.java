package Homework08;

import java.util.Scanner;

public class Homework08 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        Human[] human = new Human[10];
        for (int i = 0; i < human.length; i++) {
            human[i] = new Human();
            System.out.print("Input a name of person " + (i + 1) + ": ");
            human[i].setName(in.next());
            System.out.print("Input a weight of person " + (i + 1) + ": ");
            human[i].setWeight(in.nextDouble());
        }

        Human.getSort(human);

        for (int i = 0; i < human.length; i++) {
            System.out.println();
            System.out.println("Name: " + human[i].getName() + " Weight: " + human[i].getWeight());
        }
    }
}

