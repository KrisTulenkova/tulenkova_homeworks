package Homework_05;

import java.util.Scanner;

public class Homeworck_05 {
    private static int minDigit = 9;


    public static void main(String[] args) {
        
        Scanner in = new Scanner(System.in);
        System.out.print("Input a number: ");
        int a = in.nextInt();
        while (a != -1) {
            while (a != 0) {
                if (a % 10 < minDigit) {
                    minDigit = a % 10;
                }
                a = a / 10;
            }
            System.out.print("Input a number: ");
            a = in.nextInt();
        }
        System.out.println("Minimum digit is: " + minDigit);
    }
}
