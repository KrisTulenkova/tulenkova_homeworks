package Homework_06;

import java.util.Arrays;

public class Homework06 {
    private static int index;

    public static int getIndex(int[] a, int b) {
        for (int i = 0; i < a.length - 1; i++) {
            if (a[i] == b) {
                index = i;
                break;
            } else {
                index = -1;
            }
        }
        return index;
    }

    public static void changeArray(int[] c) {

        for (int j = 0; j < c.length - 1; j++) {
            for (int i = 0; i < c.length - 1; i++) {
                if (c[i] == 0) {
                    c[i] = c[i + 1];
                    c[i + 1] = 0;
                }
            }
        }
    }

    public static void main(String[] args) {
        
        int[] a = { 12, 0, 0, 14, 15, 0, 18, 0, 0, -1, 20 };
        int[] c = { 34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20 };

        changeArray(c);
        
        System.out.println("Solving the first task:");
        System.out.println("Index: " + getIndex(a, 12));
        System.out.println();
        System.out.println("Solving the second task::");
        System.out.println("New array:" + Arrays.toString(c));
    }
}
